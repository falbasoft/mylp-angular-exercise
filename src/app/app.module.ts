import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SlideshowModule } from 'ng-simple-slideshow';

import {
  MatToolbarModule,
  MatCardModule,
  MatSelectModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatButtonModule,
  MatIconModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatListModule,
} from '@angular/material';

import { PitchesComponent } from './components/pitches/pitches.component';
import { PitchComponent } from './components/pitch/pitch.component';
import { SearchComponent } from './components/search/search.component';
import { AlertComponent } from './components/alert/alert.component';
import { PitchDetailsComponent } from './components/pitch-details/pitch-details.component';
import { PitchSlotsComponent } from './components/pitch-slots/pitch-slots.component';

import { FilterByAvailabilityPipe } from './filters/filter-by-availability.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PitchesComponent,
    PitchComponent,
    SearchComponent,
    AlertComponent,
    PitchDetailsComponent,
    PitchSlotsComponent,
    FilterByAvailabilityPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatToolbarModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatListModule,
    BrowserAnimationsModule,
    SlideshowModule,
  ],
  providers: [
    MatNativeDateModule,
  ],
  entryComponents: [
    AlertComponent,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
