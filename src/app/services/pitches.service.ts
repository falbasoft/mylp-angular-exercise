import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of, EMPTY } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PitchesService {
  private apiUrl = environment.apiRoot + '/pitches';

  constructor(private http: HttpClient) {}

  getPitch(id: string): Observable<{}> {
    return this.http.get<{}>(this.apiUrl + '/' + id).pipe(
      map(response => response['data']),
      catchError(this.handleError<[]>('getPitch', []))
    );
  }

  getPitches(params: {}): Observable<[]> {
    if (params['city'] && params['name']) {
      return this.http.get<{}>(
        this.apiUrl
        + '?filter[city]=' + params['city']
        + '&filter[name]=' + params['name']
        + (params['sport'] ? '&filter[sport]=' + params['sport'].toLowerCase() : '')
        + (params['starts'] ? '&filter[starts]=' + params['starts'] + ' 00:00' : '')
        + (params['ends'] ? '&filter[ends]=' + params['ends'] + ' 23:59' : '')
      ).pipe(
        map(response => response['data']),
        catchError(this.handleError<[]>('getPitches', []))
      );
    } else {
      return EMPTY;
    }
  }

  getSlots(id: string, starts: string, ends: string) {
    if (starts && ends) {
      return this.http.get<{}>(
        this.apiUrl + '/' + id + '/slots?filter[starts]=' + starts + '&filter[ends]=' + ends
      ).pipe(
        map(response => response['data']),
        catchError(this.handleError<[]>('getPitches', []))
      )
    } else {
      return EMPTY;
    }
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }
}
