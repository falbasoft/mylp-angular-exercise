import { Injector } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { environment } from '../../environments/environment';

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { PitchesService } from './pitches.service';

describe('PitchesService', () => {
  let service: PitchesService;
  let httpMock: HttpTestingController;
  let injector: Injector;

  let apiUrl = environment.apiRoot + '/pitches';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ PitchesService ],
    });
  });

  beforeEach(() => {
    service = TestBed.get(PitchesService)
    httpMock = TestBed.get(HttpTestingController);
  });

  // Make sure no external calls are made
  afterEach(() => httpMock.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to get a pitch by id', () => {
    service.getPitch('123').subscribe();

    httpMock.expectOne(apiUrl + '/123').flush('');
  });

  it('should be able to get a list of pitches for given params', () => {
    service.getPitches({
      city: 'city',
      name: 'name',
      sport: 'sport',
      starts: 'starts',
      ends: 'ends',
    });

    // Check all the parameters are correct
    httpMock.match(req => req.url === apiUrl
      + '/?filter[city]=city'
      + '&filter[name]=name'
      + '&filter[sport]=sport'
      + '&filter[starts]=starts 00:00'
      + '&filter[ends]=ends 23:59');

    httpMock.expectNone(apiUrl);
  });

  it('should be able to get a list of slots given params', () => {
    service.getSlots('123', 'starts', 'ends');

    // Check all the parameters are correct
    httpMock.match(req => req.url === apiUrl + '/123'
      + '/?filter[starts]=starts'
      + '&filter[ends]=ends');

    httpMock.expectNone(apiUrl + '/123');
  });
});
