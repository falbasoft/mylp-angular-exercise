import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filterByAvailability' })
export class FilterByAvailabilityPipe implements PipeTransform {
  transform(items: []): {} {
    if (!items) {
      return items;
    }

    return items.filter(item => parseInt(item['attributes']['availabilities']) > 0)
  }
}

