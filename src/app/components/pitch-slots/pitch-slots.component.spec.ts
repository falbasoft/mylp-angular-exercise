import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';

import { PitchSlotsComponent } from './pitch-slots.component';
import { PitchesService } from '../../services/pitches.service';

import { FilterByAvailabilityPipe } from '../../filters/filter-by-availability.pipe';

describe('PitchSlotsComponent', () => {
  let component: PitchSlotsComponent;
  let fixture: ComponentFixture<PitchSlotsComponent>;

  beforeEach(async(() => {
    // Service stubs to return mock data

    let activatedRouteStub = {
      paramMap: of({
        get: id => '123'
      })
    }

    let pitchesServiceStub = {
      getSlots: (id, starts, ends) => of([
        {
          attributes: {
            starts: '2018-01-09T06:40:00+00:00',
            ends: '2018-01-09T07:20:00+00:00',
            price: '12.05',
            admin_fee: '0.05',
            currency: 'GBP',
            availabilities: 0
          }
        },
        {
          attributes: {
            starts: '2018-01-09T06:40:00+00:00',
            ends: '2018-01-09T07:20:00+00:00',
            price: '12.05',
            admin_fee: '0.05',
            currency: 'GBP',
            availabilities: 2
          }
        }
      ]),
    }

    TestBed.configureTestingModule({
      declarations: [
        PitchSlotsComponent,
        FilterByAvailabilityPipe,
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: PitchesService, useValue: pitchesServiceStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain list item', () => {
    const li = fixture.nativeElement.querySelector('li');

    expect(li).toBeTruthy();
  });

  it('should contain correctly formatted start date', () => {
    const h4 = fixture.nativeElement.querySelector('h4');

    expect(h4.textContent).toContain('2018-01-09');
  });

  it('should contain duration', () => {
    const duration = fixture.nativeElement.querySelector('.duration');

    expect(duration.textContent).toMatch('40 minutes');
  });

  it('should contain slots', () => {
    const slots = fixture.nativeElement.querySelector('.slots');

    expect(slots.textContent).toMatch('Slots: 2');
  });

  it('should contain price', () => {
    const price = fixture.nativeElement.querySelector('.price');

    expect(price.textContent).toContain('12.10');
  });
});
