import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import * as moment from 'moment';

import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { PitchesService } from '../../services/pitches.service';

@Component({
  selector: 'app-pitch-slots',
  templateUrl: './pitch-slots.component.html',
  styleUrls: ['./pitch-slots.component.scss']
})

export class PitchSlotsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private pitchesService: PitchesService,
  ) {}

  private slots: Observable<[]>

  slotStarts(attributes: {}) {
    return moment(attributes['starts']).format('YYYY-MM-DD h:mm');
  }

  slotDuration(attributes: {}) {
    return moment.duration(moment(attributes['ends']).valueOf() - moment(attributes['starts']).valueOf()).humanize();
  }

  slotPrice(attributes: {}) {
    return (parseFloat(attributes['price']) + parseFloat(attributes['admin_fee'])).toFixed(2);
  }

  ngOnInit() {
    this.slots = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.pitchesService.getSlots(params.get('id'), params.get('starts'), params.get('ends'))
      ),
    );
  }

}
