import { Router, ActivatedRoute } from '@angular/router';
import { Component, Input } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';

import {
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatIconModule,
  MatDialogModule,
  MatDialog,
} from '@angular/material';

import { SearchComponent } from './search.component';
import { AlertComponent } from '../alert/alert.component';
import { PitchesService } from '../../services/pitches.service';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let dialogSpy: jasmine.Spy;

  // Service stubs to return mock data
  let activatedRouteStub = {
    paramMap: of({
      get: id => '123'
    }),
    snapshot: {
      paramMap: {
        get: id => '123'
      }
    }
  }

  let pitchesServiceStub = {
    getPitches: ({}) => of(['p1', 'p2']),
  }

  // Dialog stub to create spy on open
  let dialogStub = {
    open: jasmine.createSpy('open')
  }

  // Router stub to create spy on navigate
  let routerStub = {
    navigate: jasmine.createSpy('navigate')
  }

  // Mocking child component
  @Component({
    selector: 'app-pitches',
    template: '<p>Pitches</p>'
  })
  class MockPitchesComponent {
    @Input() pitches: {};
    @Input() starts: string;
    @Input() ends: string;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchComponent,
        AlertComponent,
        MockPitchesComponent,
      ],
      imports: [
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatIconModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: PitchesService, useValue: pitchesServiceStub },
        { provide: Router, useValue: routerStub },
        { provide: MatDialog, useValue: dialogStub },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show alert when no name provided', () => {
    delete component['name'];
    component.search();

    expect(dialogStub.open).toHaveBeenCalledWith(jasmine.any(Function), {
      data: {
        content: 'Please provide a name of the pitch to be searched.'
      }
    });
  });

  it('should show alert when no ends date is before starts date', () => {
    component['name'] = 'name';
    component['starts'] = '2019-10-11';
    component['ends'] = '2019-10-10';

    component.search();

    expect(dialogStub.open).toHaveBeenCalledWith(jasmine.any(Function), {
      data: {
        content: 'Please make sure starts date is before ends date.',
      }
    });
  });

  it('should show alert when interval between dates is more than 14 days', () => {
    component['name'] = 'name';
    component['starts'] = '2019-10-10';
    component['ends'] = '2019-10-25';

    component.search();

    expect(dialogStub.open).toHaveBeenCalledWith(jasmine.any(Function), {
      data: {
        content: 'Please make sure that interval is less than 14 days.',
      }
    });
  });

  it('should navigate with proper parameters', () => {
    component['name'] = 'name';
    component['starts'] = '2019-10-10';
    component['ends'] = '2019-10-11';

    component.search();

    expect(routerStub.navigate).toHaveBeenCalledWith(['pitches', '123', 'name', { sport: '123', starts: '2019-10-10', ends: '2019-10-11' }]);
  });
});
