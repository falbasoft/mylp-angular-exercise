import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as moment from 'moment';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { PitchesService } from '../../services/pitches.service';
import { AlertComponent } from '../alert/alert.component';

export const DATE_FORMAT = 'YYYY-MM-DD';

export const MY_FORMATS = {
  parse: {
    dateInput: DATE_FORMAT,
  },
  display: {
    dateInput: DATE_FORMAT,
    monthYearLabel: 'YYYY MMM',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY MMMM',
  },
};

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class SearchComponent implements OnInit {
  // Predefined list of sports
  private sports = [ 'Athletics', 'Badminton', 'Cricket', 'eSports', 'Football', 'Futsal', 'Golf', 'Gym', 'Hockey', 'Netball', 'Rugby', 'Space Hire', 'Squash', 'Swimming', 'Table Tennis', 'Tennis' ];

  // Predefined list of cities
  private cities = [ 'Brighton', 'Dublin', 'London', 'Luton', 'Manchester', 'Rest Of The UK' ];

  private sport: string;
  private city: string;
  private name: string;
  private starts: string;
  private ends: string;

  private pitches: Observable<{}>;

  constructor(
    private pitchesService: PitchesService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  // Search action
  search() {
    if (this.name) {
      // Proceed only if name provided
      let params = {};

      if (this.starts && this.ends) {
        if (moment(this.starts).valueOf() > moment(this.ends).valueOf()) {
          // Show alert when starts date is after ends date
          this.dialog.open(AlertComponent, {
            data: {
              content: 'Please make sure starts date is before ends date.',
            }
          });

          return;
        }

        if (moment(this.ends).valueOf() - moment(this.starts).valueOf() > 1.21e+9) {
          // Show alert when interval is more than 14 days
          this.dialog.open(AlertComponent, {
            data: {
              content: 'Please make sure that interval is less than 14 days.',
            }
          });

          return;
        }
      }

      if (this.sport) params['sport'] = this.sport;
      if (this.starts) params['starts'] = moment(this.starts).format(DATE_FORMAT)
      if (this.ends) params['ends'] = moment(this.ends).format(DATE_FORMAT)

      this.router.navigate(['pitches', this.city, this.name, params]);
    } else {
      // Show alert when no name provided
      this.dialog.open(AlertComponent, {
        data: {
          content: 'Please provide a name of the pitch to be searched.',
        }
      });
    }
  }

  // Execute search on input enter
  searchOnEnter(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }

  ngOnInit() {
    // Read initial parameters statically
    this.sport = this.route.snapshot.paramMap.get('sport');
    this.city = this.route.snapshot.paramMap.get('city') || 'London';
    this.name = this.route.snapshot.paramMap.get('name');
    this.starts = this.route.snapshot.paramMap.get('starts');
    this.ends = this.route.snapshot.paramMap.get('ends');

    // Observe search parameters change
    this.pitches = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.pitchesService.getPitches({
          city: params.get('city'),
          name: params.get('name'),
          sport: params.get('sport'),
          starts: params.get('starts'),
          ends: params.get('ends'),
        })
      )
    );
  }
}
