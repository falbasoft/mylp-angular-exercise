import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';

import { SlideshowModule } from 'ng-simple-slideshow';

import {
  MatCardModule,
  MatProgressSpinnerModule,
} from '@angular/material';

import { PitchDetailsComponent } from './pitch-details.component';
import { PitchesService } from '../../services/pitches.service';

describe('PitchDetailsComponent', () => {
  let component: PitchDetailsComponent;
  let fixture: ComponentFixture<PitchDetailsComponent>;

  // Mocking child component
  @Component({
    selector: 'app-pitch-slots',
    template: '<p>Mock Pitch Slots</p>'
  })
  class MockPitchSlotsComponent {}

  beforeEach(async(() => {
    // Service stubs to return mock data

    let activatedRouteStub = {
      paramMap: of({
        get: id => '123'
      })
    }

    let pitchesServiceStub = {
      getPitch: id => of({
        attributes: {
          name: 'name',
          sport: 'sport',
          about: 'about',
          images: {
            large: ['1.jpg', '2.jpg', '3.jpg']
          },
          facilities: ['a', 'b', 'c'],
          payment: [
            {key: 'k1', value: 'v1'},
            {key: 'k2', value: 'v2'},
          ]
        }
      }),
    }

    TestBed.configureTestingModule({
      declarations: [
        PitchDetailsComponent,
        MockPitchSlotsComponent,
      ],
      imports: [
        SlideshowModule,
        MatCardModule,
        MatProgressSpinnerModule,
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: PitchesService, useValue: pitchesServiceStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain name', () => {
    const h1 = fixture.nativeElement.querySelector('h1');

    expect(h1.textContent).toMatch('name');
  });

  it('should contain sport', () => {
    const h2 = fixture.nativeElement.querySelector('h2');

    expect(h2.textContent).toMatch('sport');
  });

  it('should contain images', () => {
    const slideshow = fixture.nativeElement.querySelector('slideshow');

    expect(slideshow.innerHTML).toContain('1.jpg');
    expect(slideshow.innerHTML).toContain('2.jpg');
    expect(slideshow.innerHTML).toContain('3.jpg');
  });

  it('should contain about', () => {
    const about = fixture.nativeElement.querySelector('#about');

    expect(about.textContent).toMatch('about');
  });

  // Of course given more time we test all the populated fields here...
});
