import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { PitchesService } from '../../services/pitches.service';

@Component({
  selector: 'app-pitch-details',
  templateUrl: './pitch-details.component.html',
  styleUrls: ['./pitch-details.component.scss'],
})

export class PitchDetailsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private pitchesService: PitchesService,
  ) {}

  private pitch: Observable<{}>

  ngOnInit() {
    // Observe pitch and its slots according to id parameter change
    this.pitch = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.pitchesService.getPitch(params.get('id'))
      ),
    );
  }
}
