import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pitches',
  templateUrl: './pitches.component.html',
  styleUrls: ['./pitches.component.scss']
})

export class PitchesComponent implements OnInit {
  @Input() pitches: {};
  @Input() starts: string;
  @Input() ends: string;

  constructor() {}

  ngOnInit() {}
}
