import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PitchesComponent } from './pitches.component';

describe('PitchesComponent', () => {
  let component: PitchesComponent;
  let fixture: ComponentFixture<PitchesComponent>;

  // Mocking child component
  @Component({
    selector: 'app-pitch',
    template: '<p>{{ pitch }}</p>'
  })
  class MockPitchComponent {
    @Input() pitch: {};
    @Input() starts: string;
    @Input() ends: string;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PitchesComponent,
        MockPitchComponent,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchesComponent);
    component = fixture.componentInstance;
    component.pitches = ['a', 'b', 'c'];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain given pitches', () => {
    const pitches = fixture.nativeElement.querySelectorAll('app-pitch');

    expect(pitches[0].querySelector('p').innerText).toMatch('a');
    expect(pitches[1].querySelector('p').innerText).toMatch('b');
    expect(pitches[2].querySelector('p').innerText).toMatch('c');
  });
});
