import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pitch',
  templateUrl: './pitch.component.html',
  styleUrls: ['./pitch.component.scss']
})

export class PitchComponent {
  @Input() pitch: {};
  @Input() starts: string;
  @Input() ends: string;

  constructor(private router: Router) {}

  // Navigate to pitch details
  showMore() {
    let params = {};

    if (this.starts) params['starts'] = this.starts;
    if (this.ends) params['ends'] = this.ends;

    this.router.navigate(['pitch', this.pitch['id'], params]);
  }
}
