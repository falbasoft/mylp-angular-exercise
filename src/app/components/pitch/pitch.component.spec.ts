import { Router } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatCardModule } from '@angular/material';

import { PitchComponent } from './pitch.component';

describe('PitchComponent', () => {
  let component: PitchComponent;
  let fixture: ComponentFixture<PitchComponent>;

  // Router stub to create spy on navigate
  let routerStub = {
    navigate: jasmine.createSpy('navigate')
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PitchComponent ],
      imports: [
        MatCardModule,
      ],
      providers: [
        { provide: Router, useValue: routerStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchComponent);
    component = fixture.componentInstance;
    component.starts = '2019-10-10';
    component.ends = '2019-10-11';
    component.pitch = {
      id: '123',
      attributes: {
        images: { small: [ 'small.jpg' ] },
        name: 'name',
        format: 'format',
        surface: 'surface',
        operator: 'operator',
      }
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain name', () => {
    const h3 = fixture.nativeElement.querySelector('h3');

    expect(h3.textContent).toMatch('name');
  });

  it('should contain format', () => {
    const format = fixture.nativeElement.querySelector('.format');

    expect(format.textContent).toMatch('Format: format');
  });

  it('should contain surface', () => {
    const surface = fixture.nativeElement.querySelector('.surface');

    expect(surface.textContent).toMatch('Surface: surface');
  });

  it('should contain operator', () => {
    const operator = fixture.nativeElement.querySelector('.operator');

    expect(operator.textContent).toMatch('Operator: operator');
  });

  it('clicking showMore should navigate to details', () => {
    component.showMore();

    expect(routerStub.navigate).toHaveBeenCalledWith(['pitch', '123', { starts: '2019-10-10', ends: '2019-10-11' }]);
  });
});
